from django.db import models

# Create your models here.

#Creando modelo Couch 
class Coach(models.Model):
    name = models.CharField(max_length=150)
    age = models.PositiveIntegerField()
    email = models.EmailField()
    rut = models.CharField(max_length=12)
    nickname = models.CharField(max_length=80)
    
    def __str__(self):
    	return self.name

#Creando modelo Team
class Team(models.Model):
    name = models.CharField(max_length=150)
    description = models.TextField()
    code = models.CharField(max_length=20, unique=True)
    logo = models.ImageField(upload_to='teams/')
    coach = models.OneToOneField(Coach, on_delete=models.CASCADE)
    def __str__(self):
    	return self.name

POSITION_PLAYER = (
	('BA','Base'),
    ('ES','Escolta'),
    ('AL','Alero'),
    ('AP','Ala-Pivot'),
    ('PI','Pivot'),
)
    
#Creando modelo Player 
class Player(models.Model):
    name = models.CharField(max_length=150)
    nickname = models.CharField(max_length=80)
    birthday = models.DateField()
    age = models.PositiveIntegerField()
    rut = models.CharField(max_length=12)
    email = models.EmailField()
    heigth = models.PositiveIntegerField(help_text='in centimeters')
    weigth = models.PositiveIntegerField(help_text='in grams')
    photo = models.ImageField(upload_to='players/')
    position = models.CharField(choices=POSITION_PLAYER, max_length=2)
    team = models.ForeignKey(Team, on_delete=models.CASCADE)

    def __str__(self):
    	return '{name} ({team})' . format(name=self.name, team=self.team)

class Roster(models.Model):
    name = models.CharField(max_length=150)
    player = models.ManyToManyField(Player)

    def __str__(self):
    	return self.name

class Match(models.Model):
    name = models.CharField(max_length=150)
    team1 = models.ForeignKey(Roster, related_name='team1', on_delete=models.CASCADE)
    team2 = models.ForeignKey(Roster, related_name='team2', on_delete=models.CASCADE)
    date = models.DateTimeField()

    def __str__(self):
    	return '{name} ({team1} VS {team2})'.format(
    		name=self.name, 
    		team1=self.team1, 
    		team2=self.team2
    	) 