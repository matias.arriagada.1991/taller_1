from django.shortcuts import render
from basket.models import Team, Player, Coach, Match
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

#def index(request):
#    user_list = User.objects.all()
#    page = request.GET.get('page', 1)

#    paginator = Paginator(user_list, 10)
#    try:
#        users = paginator.page(page)
#    except PageNotAnInteger:
#        users = paginator.page(1)
#    except EmptyPage:
#        users = paginator.page(paginator.num_pages)
#
#    return render(request, 'core/user_list.html', { 'users': users })

def basket(request):
	data = {}
	template_name = 'basket.html'
	
	data['title'] = 'List Matchs'

	page = request.GET.get('page', 1)
	match_lists = Match.objects.all()[:5]
	#pasar otro diccionario a la misma vista
	player_lists = Player.objects.all()[:5]
	#data['player'] = Player.objects.all()
	
	paginator = Paginator(match_lists, 50)
	try:
		data['matchs'] = paginator.page(page)
	except PageNotAnInteger:
		data['matchs'] = paginator.page(1)
	except EmptyPage:
		data['matchs'] = paginator.page(paginator.num_pages)

	paginator = Paginator(player_lists, 5)
	try:
		data['players'] = paginator.page(page)
	except PageNotAnInteger:
		data['players'] = paginator.page(1)
	except EmptyPage:
		data['players'] = paginator.page(paginator.num_pages)

	return render(request, template_name, data)

#def basket_team(request):
#	data = {}
#	template_name = 'basket_team.html'
#	
#	data['title'] = 'List Teams'
#
#	page = request.GET.get('page', 1)
#	team_lists = Team.objects.all()
#
#	paginator = Paginator(team_lists, 3)
#
#	try:
#		data['teams'] = paginator.page(page)
#	except PageNotAnInteger:
#		data['teams'] = paginator.page(1)
#	except EmptyPage:
#		data['teams'] = paginator.page(paginator.num_pages)
#
#	return render(request, template_name, data)

def team(request):
	data = {}
	template_name = 'team.html'
	
	data['title'] = 'List Teams'

	page = request.GET.get('page', 1)
	team_lists = Team.objects.all()

	paginator = Paginator(team_lists, 3)

	try:
		data['teams'] = paginator.page(page)
	except PageNotAnInteger:
		data['teams'] = paginator.page(1)
	except EmptyPage:
		data['teams'] = paginator.page(paginator.num_pages)

	return render(request, template_name, data)

def player(request):
	data = {}
	template_name = 'player.html'
	
	data['title'] = 'List Players'
	#data['player'] = Player.objects.all()

	page = request.GET.get('page', 1)
	player_lists = Player.objects.all()
	team_lists = Team.objects.all()
	#player_count = Player.objects.all().count

	paginator = Paginator(team_lists, 50)
	try:
		data['teams'] = paginator.page(page)
	except PageNotAnInteger:
		data['teams'] = paginator.page(1)
	except EmptyPage:
		data['teams'] = paginator.page(paginator.num_pages)
	

	paginator = Paginator(player_lists, 50)
	try:
		data['players'] = paginator.page(page)
	except PageNotAnInteger:
		data['players'] = paginator.page(1)
	except EmptyPage:
		data['players'] = paginator.page(paginator.num_pages)

	return render(request, template_name, data)

def coach(request):
	data = {}
	template_name = 'coach.html'
	
	data['title'] = 'List Coachs'
	#data['coach'] = Player.objects.all()

	page = request.GET.get('page', 1)
	team_lists = Team.objects.all()

	
	paginator = Paginator(team_lists, 50)
	try:
		data['teams'] = paginator.page(page)
	except PageNotAnInteger:
		data['teams'] = paginator.page(1)
	except EmptyPage:
		data['teams'] = paginator.page(paginator.num_pages)

	return render(request, template_name, data)

def match(request):
	data = {}
	template_name = 'match.html'
	
	data['title'] = 'List Matchs'
	#data['coach'] = Player.objects.all()

	page = request.GET.get('page', 1)
	match_lists = Match.objects.all()

	
	paginator = Paginator(match_lists, 50)
	try:
		data['matchs'] = paginator.page(page)
	except PageNotAnInteger:
		data['matchs'] = paginator.page(1)
	except EmptyPage:
		data['matchs'] = paginator.page(paginator.num_pages)

	return render(request, template_name, data)