from django.urls import path
from basket import views

urlpatterns = [
	#path('', views.basket_team, name='basket_list'),
	path('', views.basket, name='basket'),
	path('teams/', views.team, name='team'),
	path('players/', views.player, name='player'),
	path('coachs/', views.coach, name='coach'),
	path('matchs/', views.match, name='match')
	]

