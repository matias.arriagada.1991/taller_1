# Generated by Django 3.0.4 on 2020-04-20 05:45

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('basket', '0005_match'),
    ]

    operations = [
        migrations.AddField(
            model_name='match',
            name='team2',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, related_name='team2', to='basket.Roster'),
            preserve_default=False,
        ),
        migrations.RemoveField(
            model_name='match',
            name='team1',
        ),
        migrations.AddField(
            model_name='match',
            name='team1',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, related_name='team1', to='basket.Roster'),
            preserve_default=False,
        ),
    ]
