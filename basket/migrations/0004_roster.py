# Generated by Django 3.0.4 on 2020-04-20 04:20

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('basket', '0003_auto_20200420_0216'),
    ]

    operations = [
        migrations.CreateModel(
            name='Roster',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=150)),
                ('player', models.ManyToManyField(to='basket.Player')),
            ],
        ),
    ]
