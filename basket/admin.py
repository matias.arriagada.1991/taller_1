from django.contrib import admin
from django.utils.safestring import mark_safe

from basket.models import (
	Coach, 
	Team, 
	Player,
	Roster,
	Match
)
# Register your models here.

@admin.register(Coach)
class CoachAdmin(admin.ModelAdmin):
	pass
@admin.register(Team)
class TeamAdmin(admin.ModelAdmin):
	#filtro de busqueda
	search_fields = ['name', ]
	list_display = (
		'name',
		'description',
		'code',
		'render_logo',
		'coach',
	)

	def render_logo(self, team):
		return mark_safe("<img src='/media/{}'  width='60' height='60'  />".format(team.logo))


@admin.register(Player)
class PlayerAdmin(admin.ModelAdmin):
	list_display = ('name', 'team_code')
	#filtro por team y cumpleaños
	list_filter = ('team', 'birthday')
	#filtro de busqueda por nombre sobrenombre y rut
	search_fields = ['name', 'nickname', 'rut']
	date_hierarchy = 'birthday'
	
	def team_code(self, obj):
		return obj.team.code

@admin.register(Roster)
class RosterAdmin(admin.ModelAdmin):
	pass
@admin.register(Match)
class MatchAdmin(admin.ModelAdmin):
	pass